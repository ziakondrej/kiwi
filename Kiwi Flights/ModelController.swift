//
//  ModelController.swift
//  Kiwi Flights
//
//  Created by Ondrej Žiak on 08/05/2018.
//  Copyright © 2018 Ondrej Žiak. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

/*
 A controller object that manages a simple model -- a collection of month names.
 
 The controller serves as the data source for the page view controller; it therefore implements pageViewController:viewControllerBeforeViewController: and pageViewController:viewControllerAfterViewController:.
 It also implements a custom method, viewControllerAtIndex: which is useful in the implementation of the data source methods, and in the initial configuration of the application.
 
 There is no need to actually create view controllers for each page in advance -- indeed doing so incurs unnecessary overhead. Given the data model, these methods create, configure, and return a new view controller on demand.
 */


class ModelController: NSObject, UIPageViewControllerDataSource {

    var pageData: [FlightData] = [FlightData()]

    override init() {
        super.init()
        /**
         Supposed to create the data model here.
         Creating data model in `RootViewController`
        */
    }

    func viewControllerAtIndex(_ index: Int, storyboard: UIStoryboard) -> DataViewController? {
        // Return the data view controller for the given index.
        if (self.pageData.count == 0) || (index >= self.pageData.count) {
            return nil
        }

        // Create a new view controller and pass suitable data.
        let dataViewController = storyboard.instantiateViewController(withIdentifier: "DataViewController") as! DataViewController
        dataViewController.index = index
        dataViewController.dataObject = self.pageData[index]
        return dataViewController
    }

    func indexOfViewController(_ viewController: DataViewController) -> Int {
        // Return the index of the given data view controller.
        // For simplicity, this implementation uses a static array of model objects and the view controller stores the model object; you can therefore use the model object to identify the index.
        return viewController.index
    }
    

    // MARK: - Page View Controller Data Source

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        var index = self.indexOfViewController(viewController as! DataViewController)
        if (index == 0) || (index == NSNotFound) {
            return nil
        }
        
        index -= 1
        return self.viewControllerAtIndex(index, storyboard: viewController.storyboard!)
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        var index = self.indexOfViewController(viewController as! DataViewController)
        if index == NSNotFound {
            return nil
        }
        
        index += 1
        if index == self.pageData.count {
            return nil
        }
        return self.viewControllerAtIndex(index, storyboard: viewController.storyboard!)
    }

    //MARK: - Networking
    
    /**
     Getting data from the API using Alamofire.
     
     - Parameter url: Url for data retrieval.
    */
    func getData (url: String) {
        Alamofire.request(url, method: .get).responseJSON {
            response in
            if response.result.isSuccess {

                // On succesful data retrieval, fill data model
                let dataJSON : JSON = JSON(response.result.value!)
                self.fillData(json: dataJSON)
                
            } else {
                print("Error \(response.result.error.debugDescription)")
            }
        }
    }
    
    
    //MARK: - JSON parsing
    
    /**
     Parsing JSON data using SwiftyJSON.
     
     - Parameter json: Raw data in JSON format.
    */
    func fillData(json : JSON) {
        if json["data"][0]["cityTo"].string != nil {
            pageData.remove(at: 0)
            
            if json["data"].count > 4 {
                for index in 0...4 {
                    fillFlightData(json: json, index: index)
                }
            }
            else {
                for index in 0...json["data"].count-1 {
                    fillFlightData(json: json, index: index)
                }
            }
        }
        else {
            print("Error, couldn't get the data")
        }
    }
    
    /**
     Retrieves data from JSON to `FlighData` object and adds object to `pageData`.
     
     - Parameter json: JSON data.
     - Parameter index: Index of current retrieved chuck of JSON data.
    */
    func fillFlightData(json: JSON, index: Int) {
        let tmp = FlightData()
        
        tmp.cityTo = json["data"][index]["cityTo"].string!
        tmp.cityFrom = json["data"][index]["cityFrom"].string!
        tmp.dDate = timeStampToString(timeStamp: json["data"][index]["dTimeUTC"].doubleValue)
        tmp.price = String(json["data"][index]["price"].doubleValue) + "€"
        tmp.duration = json["data"][index]["fly_duration"].string!
        tmp.stepOvers = String(json["data"][index]["route"].count - 1)
        tmp.imageID = json["data"][index]["mapIdto"].string!
        
        pageData.append(tmp)
    }
    
    /**
     Convert unix timestamp from JSON data to usable string.
     
     - Parameter timeStamp: Timestamp value.
     - Returns: Date in "E, MMM d, yyyy HH:mm Z" format as a `String`.
     */
    func timeStampToString(timeStamp: Double) -> String {
        let date = Date(timeIntervalSince1970: timeStamp)
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "E, MMM d, yyyy HH:mm Z"
        let strDate = dateFormatter.string(from: date)
        
        return strDate
    }
}

