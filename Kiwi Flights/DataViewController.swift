//
//  DataViewController.swift
//  Kiwi Flights
//
//  Created by Ondrej Žiak on 08/05/2018.
//  Copyright © 2018 Ondrej Žiak. All rights reserved.
//

import UIKit

class DataViewController: UIViewController {

    @IBOutlet weak var pageControll: UIPageControl!
    @IBOutlet weak var destinationLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var flyDateLabel: UILabel!
    @IBOutlet weak var cityFromLabel: UILabel!
    @IBOutlet weak var stepoverLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var destinationImage: UIImageView!
    
    var index: Int = 0
    var dataObject: FlightData = FlightData()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // Assigning values to UI elements
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.pageControll.currentPage = index
        self.destinationLabel.text = dataObject.cityTo
        self.priceLabel.text = dataObject.price
        self.flyDateLabel.text = dataObject.dDate
        self.cityFromLabel.text = "From: " + dataObject.cityFrom
        self.stepoverLabel.text = "Stepovers: " + dataObject.stepOvers
        self.durationLabel.text = "Travel duration: " + dataObject.duration
        let imageURL = "https://images.kiwi.com/photos/600/\(self.dataObject.imageID).jpg"
        self.destinationImage.load(url: URL(string: imageURL)!)
    }


}

