//
//  UIImageView+downloadedFrom.swift
//  Job Interview Project
//
//  Created by Ondrej Žiak on 22/04/2018.
//  Copyright © 2018 Ondrej Žiak. All rights reserved.
//

import UIKit

extension UIImageView {
    
    // FIXME: use better way to load image
    
    /**
     Loads image from url.

     Parameter url: Image url.
    */
    func load(url: URL) {
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
        }
    }
}
