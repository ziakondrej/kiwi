//
//  FlightData.swift
//  Kiwi Flights
//
//  Created by Ondrej Žiak on 09/05/2018.
//  Copyright © 2018 Ondrej Žiak. All rights reserved.
//

import Foundation

/*
 Data structure to contain data about flights in one place
 */
class FlightData {
    var cityTo: String = ""
    var cityFrom: String = ""
    var price: String = ""
    var dDate: String = ""
    var duration: String = ""
    var stepOvers: String = ""
    var imageID: String = ""
}
