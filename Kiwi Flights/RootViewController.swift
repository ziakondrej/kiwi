//
//  RootViewController.swift
//  Kiwi Flights
//
//  Created by Ondrej Žiak on 08/05/2018.
//  Copyright © 2018 Ondrej Žiak. All rights reserved.
//

// FIXME: Limit number of shown locations per day

import UIKit

class RootViewController: UIViewController, UIPageViewControllerDelegate {

    var pageViewController: UIPageViewController?
    let destinations: [String] = ["AR", "AU", "BR", "CN", "EG",
                                  "FR", "GR", "JM", "NZ", "SA",
                                  "ZA", "GB", "US", "UY", "VN"]

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        // Configure the page view controller and add it as a child view controller.
        self.pageViewController = UIPageViewController(transitionStyle: .pageCurl, navigationOrientation: .horizontal, options: nil)
        self.pageViewController!.delegate = self

        // Calling Alamofire to fetch data from API
        self.modelController.getData(url: composeURL())
        
        // FIXME: use notifications to start loading UI after Alamofire finished
        
        // Using `DispatchQueue` to wait for Alamofire to load data
        DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
            let startingViewController: DataViewController = self.modelController.viewControllerAtIndex(0, storyboard: self.storyboard!)!
            let viewControllers = [startingViewController]
            self.pageViewController!.setViewControllers(viewControllers, direction: .forward, animated: false, completion: {done in })
            
            self.pageViewController!.dataSource = self.modelController
            
            self.addChildViewController(self.pageViewController!)
            self.view.addSubview(self.pageViewController!.view)
            
            // Set the page view controller's bounds using an inset rect so that self's view is visible around the edges of the pages.
            var pageViewRect = self.view.bounds
            if UIDevice.current.userInterfaceIdiom == .pad {
                pageViewRect = pageViewRect.insetBy(dx: 40.0, dy: 40.0)
            }
            self.pageViewController!.view.frame = pageViewRect
            
            self.pageViewController!.didMove(toParentViewController: self)
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    var modelController: ModelController {
        // Return the model controller object, creating it if necessary.
        // In more complex implementations, the model controller may be passed to the view controller.
        if _modelController == nil {
            _modelController = ModelController()
        }
        return _modelController!
    }

    var _modelController: ModelController? = nil

    // MARK: - UIPageViewController delegate methods

    func pageViewController(_ pageViewController: UIPageViewController, spineLocationFor orientation: UIInterfaceOrientation) -> UIPageViewControllerSpineLocation {
        if (orientation == .portrait) || (orientation == .portraitUpsideDown) || (UIDevice.current.userInterfaceIdiom == .phone) {
            // In portrait orientation or on iPhone: Set the spine position to "min" and the page view controller's view controllers array to contain just one view controller. Setting the spine position to 'UIPageViewControllerSpineLocationMid' in landscape orientation sets the doubleSided property to true, so set it to false here.
            let currentViewController = self.pageViewController!.viewControllers![0]
            let viewControllers = [currentViewController]
            self.pageViewController!.setViewControllers(viewControllers, direction: .forward, animated: true, completion: {done in })

            self.pageViewController!.isDoubleSided = false
            return .min
        }

        // In landscape orientation: Set set the spine location to "mid" and the page view controller's view controllers array to contain two view controllers. If the current page is even, set it to contain the current and next view controllers; if it is odd, set the array to contain the previous and current view controllers.
        let currentViewController = self.pageViewController!.viewControllers![0] as! DataViewController
        var viewControllers: [UIViewController]

        let indexOfCurrentViewController = self.modelController.indexOfViewController(currentViewController)
        if (indexOfCurrentViewController == 0) || (indexOfCurrentViewController % 2 == 0) {
            let nextViewController = self.modelController.pageViewController(self.pageViewController!, viewControllerAfter: currentViewController)
            viewControllers = [currentViewController, nextViewController!]
        } else {
            let previousViewController = self.modelController.pageViewController(self.pageViewController!, viewControllerBefore: currentViewController)
            viewControllers = [previousViewController!, currentViewController]
        }
        self.pageViewController!.setViewControllers(viewControllers, direction: .forward, animated: true, completion: {done in })

        return .mid
    }

    // MARK: - Url setup
    
    /**
     This method prepares url for the API
     
     - Returns: Finished url as a `String`
     */
    func composeURL() -> String {
        let url: String = "https://api.skypicker.com/flights?v=2&sort=popularity&asc=0&flyFrom=CZ&typeFlight=oneway&one_per_date=0&oneforcity=1&wait_for_refresh=0&adults=1&limit=20"
        
        // Getting current date and date one month later for the url
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        
        let dateFrom = "&dateFrom=" + formatter.string(from: date)
        
        // Adding one month to current `Date()`
        let newDate = Calendar.current.date(byAdding: .month, value: 1, to: Date())
        let dateTo = "&dateTo=" + formatter.string(from: newDate!)
        
        // Getting random destination code from set of destinations
        let to = "&to=" + destinations[Int(arc4random_uniform(UInt32(destinations.count)))]
        print(url+dateFrom+dateTo+to)
        
        return url+dateFrom+dateTo+to
    }
}

