Not the typical README file.

####

NOTES from author:

Project completed to the best of my ability.

Offer limitation per day NOT WORKING. Due to lack of experience in permanent data storage and lack of time before finals, this part of task left incomplete. 

Usage of Alamofire and SwiftyJSON preferred over writing own code due to same reasons as stated before (lack of experience and time).



#### 

KNOWN BUGS: 

Data retrieval doesn't always deliver 5 offers.

Some image's url not working (e.g. https://images.kiwi.com/photos/600/paris_fr.jpg)



####

KNOWN PROBLEMS (to be fixed/learnt in coming months):

Usage of storyboards over coding UI

No parser

Using 3rd party libraries over own networking and parsing